import {View, Text, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import List from '../components/List';

const DaftarMobil = () => {
  return (
    <ScrollView style={styles.Container}>
      <View style={styles.Content}>
        <Text style={styles.Title}>Daftar Mobil</Text>
        <List />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor: '#fff',
  },
  Content: {
    marginTop: 30,
    alignItems: 'center',
  },
  Title: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000',
    paddingVertical: 18,
    paddingHorizontal: 16,
    alignSelf: 'flex-start',
  },
});

export default DaftarMobil;
