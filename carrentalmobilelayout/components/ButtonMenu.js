import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const ButtonMenu = ({Name, Caption}) => {
  return (
    <TouchableOpacity style={styles.Button}>
      <View style={styles.IconContainer}>
        <Icon name={Name} size={25} color={'#5CB85F'}></Icon>
      </View>
      <Text style={styles.MenusText}>{Caption}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Button: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  IconContainer: {
    height: 56,
    width: 56,
    backgroundColor: '#DEF1DF',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  MenusText: {
    fontSize: 12,
    marginTop: 8,
    color: '#000',
  },
});

export default ButtonMenu;
